#!/usr/bin/sh

sass --watch css/main.scss css/main.css &
live-server . &
disown &

echo "Running local live server!" &
